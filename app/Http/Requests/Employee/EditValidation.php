<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->id);
        return [
            'department_id' => 'required',
            'name' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'mobile_number'=>'required|regex:/^98[0-9]{8}$/',
            'email' => 'required|email',
            'address' => 'required',
//            'join_date' => 'required|min:'.$this->request->get('dob'),
            'about' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'mobile_number.regex'  => 'Enter Valid Mobile Number (eg 98xxxxxxxx)',
            'join_date.min' => 'Enter valid join date i.e should be greater than Date of Birth',
        ];
    }
}
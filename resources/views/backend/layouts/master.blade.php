<!DOCTYPE html>
<html>

@include('backend.includes.head')

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    @include('backend.includes.header')

    @include('backend.includes.menu')

    @yield('content')

    @include('backend.includes.footer')

</div>
<!-- ./wrapper -->

@include('backend.includes.js')

</body>
</html>
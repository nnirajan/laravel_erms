<div class="box-body">
    <div class="form-group">
        {{ Form::label('department_id', 'Department', ['class' => 'control-label']) }}
        {{ Form::select('department_id',$data['department_id'],null,['class' => 'form-control' ] ) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'department_id'])
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Employee Name', ['class' => 'control-label']) }}
        {{ Form::text('name', null, ['class' => 'form-control','placeholder' => 'Enter Employee Name']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'name'])
    </div>

    <div class="form-group">
        @php $today=date('Y-m-d') @endphp
        {{ Form::label('dob', 'Date of Birth', ['class' => 'control-label' ]) }}
        {{ Form::date('dob', null, ['class' => 'form-control',"max"=>$today ]) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'dob','fieldname'=>'date_error'])
    </div>

    <div class="form-group">
        {{ Form::label('gender',null, ['class' => 'control-label']) }}
        <label class="radio-inline"> {!! Form::radio('gender', 'Male', false) !!}Male </label>
        <label class="radio-inline"> {!! Form::radio('gender', 'Female', false) !!}Female </label>
        @include('backend.includes.form_field_validation',['fieldname'=>'gender'])

    </div>

    <div class="form-group">
        {{ Form::label('mobile_number', null, ['class' => 'control-label']) }}
        {{ Form::text('mobile_number', null, ['class' => 'form-control','placeholder' => 'Enter Mobile Number']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'mobile_number'])
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email Address', ['class' => 'control-label']) }}
        {{ Form::email('email', null, ['class' => 'form-control','placeholder' => 'Enter Email Address']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'email'])
    </div>

    <div class="form-group">
        {{ Form::label('address', null, ['class' => 'control-label']) }}
        {{ Form::text('address', null, ['class' => 'form-control','placeholder' => 'Enter Address']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'address'])
    </div>

    @if(isset($data['employee']))
        <div class="form-group">
            {!! Form::label('photo', 'Photo Image') !!}<br>
            @if($data['employee']->photo)
                <img src="{{ asset('images/employee/'.$data['employee']->photo) }}" alt="" height="100" width="100" class="img-responsivess">
            @else
                <p>No photo</p>
            @endif
        </div>
    @endif
    <div class="form-group">
        {{ Form::label('photo', null, ['class' => 'control-label']) }}
        {{ Form::file('employee_photo', null, ['class' => 'form-control']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'employee_photo'])
    </div>

    <div class="form-group">
        {{ Form::label('join_date', null, ['class' => 'control-label']) }}
        {{ Form::date('join_date', null, ['class' => 'form-control',"max"=>$today ]) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'join_date'])
    </div>

    <div class="form-group">
        {{ Form::label('about',null, ['class' => 'control-label']) }}
        {{ Form::textarea('about', null, ['class' => 'form-control ckeditor']) }}
        @include('backend.includes.form_field_validation',['fieldname'=>'about'])
    </div>

    <div class="form-group">
        {{ Form::label('status',null, ['class' => 'control-label']) }}
        <label class="radio-inline"> {!! Form::radio('status', '1', false) !!}Active </label>
        <label class="radio-inline"> {!! Form::radio('status', '0', true) !!}De-active </label>
    </div>
</div>
<!-- /.box-body -->
@extends('backend.layouts.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <tr>
                                                <th width="25%">Title</th>
                                                <td>{{$data['department']->title}}</td>
                                            </tr>
                                            <tr>
                                                <th width="25%">Description</th>
                                                <td>{!! $data['department']->description !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                @if($data['department']->status == 1)
                                                    <td><span class="label label-success">Active</span></td>
                                                @else
                                                    <td><span class="label label-warning">De-active</span></td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="{{ route('backend.department.edit', ['id' => $data['department']->id]) }}" class="btn btn-primary">Edit</a>
                                                </td>
                                                <td>
                                                    {!! Form::open(['method'=>'POST','route'=>['backend.department.destroy',$data['department']->id] ]) !!}
                                                        {{ method_field('DELETE') }}
                                                        {{ Form::submit('Delete', ['class'=>'btn btn-danger','onclick'=>"return confirm('Are you sure to delete this Employee?')" ] ) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->

@endsection
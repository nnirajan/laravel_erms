@extends('backend.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $data['title'] }}
                <small><a href="{{ route('backend.department.index') }}" class="btn btn-info">List Department</a>
                </small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        {{-- dispaly errors in the form --}}
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        <!-- form start -->
                        {!! Form::model($data['department'], ['route' => ['backend.department.update', $data['department']->id],'method' => 'POST']) !!}
                            {{ method_field('PUT') }}
                            @include('backend.department.includes.main_form')
                            <div class="box-footer">
                                {!! Form::submit('Update',['class'=> 'btn btn-primary']) !!}
                            </div>
                        {{ Form::close() }}
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection